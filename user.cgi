#!/usr/bin/perl
use warnings;
use strict;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;
use Data::Dumper;
use USK;

require 'user-ssh-keys-lib.pl';

our %config;
our %text;
our %in;

ReadParse();
init_config();

#my $conf = usk_get_config();

my $usk = USK->new(\%config);
my $cgi = USK::CGI->new( usk => $usk, action => 'user_list', %in );

my USK::Users $users = $usk->users();

ui_print_header(undef, $text{user_title}, "", 'user');

if ($cgi->user) {
    my USK::User $user = $cgi->user;

    print "<p><b>Authorized Keys File:</b> " . $user->authorized_keys->filename . "</p>\n";

    #print '<code style="white-space: pre-wrap">' . Dumper(\%config) . '</code>';
    #print '<code style="white-space: pre-wrap">' . Dumper($usk) . '</code>';

    print "<h2>" . $user->fullname . ' (' . $user->username . ', ' . $user->uid . ")</h2>";

    print ui_columns_start(
        [
            '',
            $text{key_name},
            $text{key_status},
            $text{key_type},
            $text{key_value}
        ],
        undef,
        0,
        [
            'style="text-align:center; width: 3em;"',
            'align="left"',
            'algin="center"',
            'align="left"',
            'align="left"'
        ],
        #$text{user_list_heading}
    );

    foreach my USK::SSHKey $key ($user->authorized_keys->keys(), $user->authorized_keys->disabled_keys()) {
        print ui_checked_columns_row(
            [
                ui_link("key.cgi?action=user-key-edit&uid=$in{uid}&key_md5=".urlize($key->md5), $key->abbrev_name),
                $key->disabled ? $text{key_status_disabled} : $text{key_status_enabled},
                $key->key_type,
                $key->abbrev_key
            ],
            [
                undef,
                $key->disabled ? 'style="font-style:italic;"' : undef,
                $key->disabled ? 'style="font-style:italic;"' : undef,
                $key->disabled ? 'style="font-style:italic;"' : undef,
                $key->disabled ? 'style="font-style:italic;"' : undef,
            ],
            'selected_keys',
            $key->md5,
        );
    }

    print ui_columns_end();

    print "<p></p>";
    print ui_form_start('key.cgi', 'post');
    print ui_hidden('action', 'user-key-add');
    print ui_hidden('uid', $user->uid);

    print ui_submit($text{button_new_key});

    print ui_form_end();

    print ui_hr();

    foreach my $system_key ($usk->system_keys->keys()) {
        system_key_form($user, $system_key);
    }

    ui_print_footer("/", $text{'index'});
}

sub system_key_form
{
    my USK::User $user = shift;
    my USK::SSHKey $key = shift;

    if ($key && $key->is_key && $key->name) {
        print ui_form_start('manage.cgi', 'post');
        print ui_hidden('action', 'user-add-system-key');
        print ui_hidden('uid', $user->uid);
        print ui_hidden('key_md5', $key->md5);
        print ui_submit($text{button_add_system_key} . ' ' . $key->name);
        print ui_form_end();
    }
}
