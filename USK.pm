BEGIN { push(@INC, ".."); };

use WebminCore;
init_config();


package USK::KeyFileLine;
use Moo;

has type => (is => 'ro', default => 'line');
has line_num => (is => 'ro');
has text => (is => 'ro');

1;

sub is_key
{
    my $self = shift;

    return $self->type eq 'key';
}

sub line
{
    return $_[0]->text. "\n";
}


package USK::SSHKeyOption;
use Moo;

has name => (is => 'ro');
has value => (is => 'ro', default => undef);
1;


sub output {
    my $self = shift;

    my $text = $self->name;

    if ($self->value) {
        $text .= '="' . $self->value . '"'
    }

    return $text;
}



package USK::SSHKey;
use Moo;
use Digest::MD5 qw(md5_base64);
use Text::ParseWords;

extends 'USK::KeyFileLine';

has type => (is => 'ro', default => 'key');
has name => (is => 'rw');
has md5 => ('is' => 'ro');
has key => (
        is => 'rw',
        coerce => sub { $_[0] =~ s/[\n\r\t]/ /g; return $_[0];},
        trigger => sub { $_[0]->{md5} = md5_base64($_[1]); }
    );
has options => ('is' => 'rw');
has disabled => (is => 'rw', default => 0);

around BUILDARGS =>
    sub {
        my ($orig, $class, @args) = @_;

        my $args = $class->$orig(@args);

        $args->{options} = [] if !$args->{options};

        if ($args->{text}) {
            my @tokens = quotewords('\s+', 1, $args->{text});

            if (!($tokens[0] =~ /^ssh-/)) {
                foreach my $option (quotewords(',', 1, shift @tokens)) {
                    if ($option =~ /^(?P<name>\w+)="*(?P<value>.*?)"*$/) {
                        $option = USK::SSHKeyOption->new(
                            name => lc($+{name}),
                            value => $+{value}
                        );

                    } else {
                        $option = USK::SSHKeyOption->new( name => lc($option) );
                    }

                    push @{$args->{options}}, $option;
                }
            }

            $args->{key} = (shift @tokens) . ' ' . (shift @tokens);
            $args->{name} = join(' ', @tokens) if !$args->{name};
        }

        return $args;
    };

1;

sub enabled
{
    return !$_[0]->disabled;
}

sub is_valid
{
    my $self = shift;

    return $self->key && $self->key =~ /^ssh-(dss|rsa|ed25519)\s+\S+/;
}

sub is_key
{
    return 1;
}

sub abbrev_key
{
    return substr($_[0]->key, 0, 40) . ' ...';
}

sub abbrev_name
{
    my $self = shift;

    if (length($self->name) > 50) {
        return substr($self->name, 0, 46) . ' ...';

    } elsif (!$self->name) {
        return 'n/a';
    }

    return $self->name;
}

sub add_option
{
    my $self = shift;
    my $option = shift;

    push @{$self->{options}}, $option;
}

sub get_option
{
    my $self = shift;
    my $name = shift;

    return (grep { $_->name eq $name } @{$self->options})[0];
}

sub has_option
{
    my $self = shift;
    my $name = shift;

    return defined $self->get_option($name);
}

sub key_type
{
    my $self = shift;

    if ($self->key =~ /^ssh-(?P<type>\S+)/) {
        return uc($+{type});

    } elsif ($self->key =~ /^ecdsa-sha2-nistp256/) {
        return 'ECDSA';

    }

    return undef;
}

sub line
{
    my $self = shift;

    my $line = ($self->disabled ? '# DISABLED: ' : '');

    my $options = '';

    foreach my $option (@{$self->options}) {
        $options .= ($options ? ',' : '') . $option->output();
    }

    $line .= $options;

    $line .= ($line ? ' ' : '') . $self->key . ' ' . $self->name . "\n";

    return $line;
}

sub is_equal
{
    my $self = shift;
    my USK::SSHKey $key = shift;

    return $self->key eq $key->key
}

sub md5_equal {
    return $_[0]->md5 eq $_[1];
}


package USK::Comment;
use Moo;

extends 'USK::KeyFileLine';

has type => (is => 'ro', default => 'comment');

1;


package USK::Other;
use Moo;

extends 'USK::KeyFileLine';

has type => (is => 'ro', default => 'other');

1;


package USK::BlankLine;
use Moo;

extends 'USK::KeyFileLine';
has type => (is => 'ro', default => 'blank_line');

1;


package USK::User;
use Moo;

has usk => (is => 'ro', required => 1);
has username => (is => 'ro');
has uid => (is => 'ro', required => 1);
has gid => (is => 'ro', required => 1);
has home => (is => 'ro', required => 1);
has real => (is => 'ro');
has authorized_keys => (
        is => 'rw',
        lazy => 1,
        builder => sub {
            return USK::AuthorizedKeysFile->new(user => $_[0]);
        }
    );

1;

sub BUILD {
    my $self = shift;

    # $self->authorized_keys(USK::AuthorizedKeysFile->new(user => $self));
}

sub fullname {
    my $self = shift;

    $self->real =~ /^([^,]*)/;

    return $1;
}

sub authorized_keys_filename
{
    my $self = shift;
    my $path = $self->usk->authorized_keys_file;

    $path =~ s/~/${$self}{home}/;

    return $path;
}

sub keys
{
    my $self = shift;

    return grep { $_->is_key() } $self->authorized_keys->keys();
}

sub get_key
{
    my $self = shift;
    my $line_num = shift;

    return $self->authorized_keys->get_line($line_num);
}

sub get_key_md5
{
    my $self = shift;
    my $md5 = shift;

    return $self->authorized_keys->get_md5($md5);
}

sub has_key
{
    my $self = shift;
    my USK::SSHKey $key = shift;

    return $self->authorized_keys->has_key($key);
}


package USK::KeyRing;
use Moo;
use Data::Dumper;

has children => (is => 'rw');
has has_changes => (is => 'rw', default => 0);

1;

sub BUILD {
    my $self = shift;

    $self->{children} = [] if !$self->children;

    $self->read();
}

sub read
{

}

sub write
{

}

sub write_changes
{
    my $self = shift;

    if ($self->has_changes) {
        $self->write();
    }
}

sub append
{
    my ($self, $item) = @_;

    push @{$self->{children}}, $item;

    $self->has_changes(1);
}

sub keys
{
    my $self = shift;

    return grep { $_->is_key() && $_->enabled } @{$self->children};
}

sub disabled_keys
{
    my $self = shift;

    return grep { $_->is_key() && $_->disabled } @{$self->children};
}

sub all_keys
{
    my $self = shift;

    return grep { $_->is_key() } @{$self->children};
}

sub update_key
{
    my $self = shift;
    my USK::SSHKey $key = shift;

    if ($self->has_key($key)) {
        $self->{children} = [ map { $_->is_key && $_->is_equal($key) ? $key : $_ } @{$self->children} ];

        $self->has_changes(1);
    }
}

sub add_key
{
    my $self = shift;
    my USK::SSHKey $key = shift;

    $self->append($key);
}

sub delete_key
{
    my $self = shift;
    my USK::SSHKey $key = shift;

    if ($self->has_key($key)) {
        $self->{children} = [grep { !$_->is_key || !$_->is_equal($key) } @{$self->children}];

        $self->has_changes(1);
    }
}

sub disable_key
{
    my $self = shift;
    my $md5 = shift;

    if (ref($md5) eq 'USK::SSHKey') {
        $md5 = $md5->md5
    }

    if ($self->has_md5($md5)) {
        foreach my USK::SSHKey $key (grep { $_->md5 eq $md5 } $self->keys()) {
            $key->disabled(1);
        }

        $self->has_changes(1);
    }
}

sub enable_key
{
    my $self = shift;
    my $md5 = shift;

    if (ref($md5) eq 'USK::SSHKey') {
        $md5 = $md5->md5
    }

    if ($self->has_md5($md5)) {
        foreach my USK::SSHKey $key (grep { $_->md5 eq $md5 } $self->disabled_keys()) {
            $key->disabled(0);
        }

        $self->has_changes(1);
    }
}

sub get_md5
{
    my $self = shift;
    my $md5 = shift;

    return (grep { $_->md5_equal($md5) } $self->all_keys())[0];
}

sub has_md5
{
    my $self = shift;
    my $md5 = shift;

    return (grep { $_->md5_equal($md5) } $self->all_keys()) > 0;
}

sub has_key
{
    my $self = shift;
    my USK::SSHKey $key = shift;

    return (grep { $_->is_equal($key) } $self->all_keys()) > 0;
}


package USK::AuthorizedKeysFile;
use Moo;
use File::Basename;
use File::stat;
use File::Path;
use Data::Dumper;
use WebminCore;

extends 'USK::KeyRing';

has user => (is => 'ro', required => 1);
has error_message => (is => 'rw');

1;

sub filename
{
    return $_[0]->user->authorized_keys_filename();
}

sub file_exists
{
    return -f $_[0]->filename;
}

sub file_permissions_ok
{
    my $self = shift;

    my $stat = stat $self->filename;

    if ($stat->mode != 010600) {
        $self->error_message('bad file permissions');

        return 0;

    } elsif ($stat->uid != $self->user->uid || $stat->gid != $self->user->gid) {
        $self->error_message('bad file owner');

        return 0;
    }

    return 1;
}

sub directory
{
    return dirname($_[0]->filename);
}

sub directory_exists
{
    return -d $_[0]->directory;
}

sub check_file_permissions
{
}

sub create_directory
{
    my $self = shift;

    if (!$self->directory_exists()) {
        File::Path::make_path(
            $self->directory(),
            {
                mode => 0700,
                owner => $self->user->uid,
                group => $self->user->gid
            }
        );
    }

    return 1;
}

sub write
{
    my $self = shift;

    $self->create_directory();

    my $file_text = '';

    #ui_print_header(undef, 'Debug', "", 'index', 1, 1);
    #print '<code style="white-space: pre-wrap">' . Dumper($self) . '</code>';

    foreach my USK::KeyFileLine $line (@{$self->children}) {
        $file_text .= $line->line();
    }

    open(my $fh, '>', $self->filename);
    print $fh $file_text;
    close $fh;

    chown $self->user->uid, $self->user->gid, $self->filename;
    chmod 0600, $self->filename;
}

sub parse_line
{
    my ($line, $line_num) = @_;

    if ($line =~ /^# DISABLED:\s*(?P<key>.*?)\s*$/i) {
        return USK::SSHKey->new(
            line_num    => $line_num,
            text        => $+{key},
            disabled    =>  1,
        );

    } elsif ($line =~ /^\s*#/) {
        return USK::Comment->new(
            line_num => $line_num,
            text => $line
        );

    } elsif ($line =~ /(^| )ssh-(?P<type>\S+)/) {
        return USK::SSHKey->new(
            line_num => $line_num,
            text => $line,
        );

    } elsif ($line =~ /^\s*$/) {
        return USK::BlankLine->new(
            line_num => $line_num,
        );
    }

    return USK::Other->new(
        line_num => $line_num,
        text => $line
    );

}

sub read
{
    my $self = shift;

    my @lines;

    my $line_num = 0;
    my $KEYFILE;
    if (open($KEYFILE, "<", $self->filename)) {
        while (my $line = <$KEYFILE>) {
            $line_num++;
            chomp $line;

            push @lines, parse_line($line, $line_num);
        }

        close $KEYFILE;
    }

    $self->children([ @lines ]);

    return @{$self->children};
}

sub get_line
{
    my $self = shift;
    my $line_num = shift;

    return (grep { $_->line_num == $line_num } @{$self->children})[0];
}

sub update_line
{
    my $self = shift;
    my $line_num = shift;
    my $line = shift;

    my $obj = parse_line($line, $line_num);

    my @lines = @{$self->children};

    splice @lines, $line_num-1, 1, $obj;

    $self->{children} = [@lines];

    $self->has_changes(1);
}

sub add_line
{
    my $self = shift;
    my $line = shift;

    my @lines = @{$self->children};

    my $obj = parse_line($line, scalar(@lines)+1);

    push @lines, $obj;

    $self->{children} = [@lines];

    $self->has_changes(1);
}

sub delete_line
{
    my $self = shift;
    my $line_num = shift;

    my @lines = grep { $_->line_num != $line_num } @{$self->children};

    $self->{children} = [@lines];

    $self->has_changes(1);
}


package USK::KeyListItem;
use Moo;

has key => (is => 'ro', required => 1);
has users => (is => 'rw');
has names => (is => 'rw');

1;

sub BUILD {
    my $self = shift;

    $self->users([]);
    $self->names([]);
}

sub add_user
{
    my $self = shift;
    my USK::User $user = shift;
    my USK::SSHKey $key = shift;

    if (!grep { $_->uid == $user->uid } @{$self->users}) {
        push @{$self->{users}}, $user;
    }

    if (!grep { $_ eq $key->name } @{$self->names}) {
        push @{$self->{names}}, $key->name;
    }
}

sub users_string
{
    my $self = shift;

    return join('<br/>', sort {$a cmp $b } map { $_->username } @{$self->users});
}

sub names_string
{
    my $self = shift;

    return join('<br/>', sort {$a cmp $b } @{$self->names})
}


package USK::KeyList;
use Moo;
use Data::Dumper;

has items => (is => 'rw');

1;

sub BUILD {
    my $self = shift;

    $self->items([]);
}

sub add_item
{
    my $self = shift;
    my $item = shift;

    push @{$self->{items}}, $item;
}

sub add_user
{
    my $self = shift;
    my USK::User $user = shift;

    foreach my $key ($user->authorized_keys->keys()) {
        my USK::KeyListItem $item = $self->get_key($key);
        if (!$item) {
            $item = USK::KeyListItem->new(key => $key);
            $self->add_item($item);
        }

        $item->add_user($user, $key);
    }
}

sub get_key
{
    my $self = shift;
    my USK::SSHKey $key = shift;

    return (grep { $_->key->is_equal($key) } @{$self->items})[0];
}


package USK::Users;
use Moo;
BEGIN { push(@INC, ".."); };

use WebminCore;
init_config();

foreign_require('useradmin');

has usk => (is =>'ro', required => 1);
has minimum_uid => (is => 'ro', default => 1000);
has maximum_uid => (is => 'ro', default => 10000);
has sort_users_by => (is => 'ro', default => 'uid');
has user_cache => (is => 'rw');

1;


sub list
{
    my $self = shift;

    if ($self->user_cache) {
        return @{$self->user_cache};
    }

    my @all_users = useradmin::list_users();
    #print '<code>';
    #print Dumper($all_users);
    #print '</code>';

    my @users;

    foreach my $user (@all_users) {
        if (($user->{uid} >= $self->minimum_uid && $user->{uid} <= $self->maximum_uid) || $user->{uid} == 0) {
            push @users, USK::User->new(usk => $self->usk, username => $user->{user}, %$user);
        }
    }

    if ($self->sort_users_by eq 'uid') {
        @users = sort { $a->uid <=> $b->uid } @users;

    } else {
        @users = sort { $a->username cmp $b->username } @users;

    }

    $self->user_cache( \@users );

    return @users;
}

sub get_user
{
    my $self = shift;
    my $uid = shift;

    return (grep { $_->uid == $uid } $self->list())[0];

}

sub get_users_with_key
{
    my $self = shift;
    my USK::SSHKey $key = shift;

    return grep { $_->has_key($key) } $self->list();
}

sub get_key_list
{
    my $self = shift;

    my $list = USK::KeyList->new();

    foreach my $user ($self->list()) {
        $list->add_user($user);
    }

    return $list;
}


package USK;
use Moo;
use Data::Dumper;

has users => (is => 'ro');
has minimum_uid => (is => 'ro', default => 1000);
has maximum_uid => (is => 'ro', default => 10000);
has sort_users_by => (is => 'ro', default => 'uid');
has authorized_keys_file => (is => 'ro');
has system_keys => (is => 'ro');

around BUILDARGS =>
    sub {
        my ($orig, $class, @args) = @_;

        my $args = $class->$orig(@args);
        my @system_keys;

        my $key_ring = USK::KeyRing->new();

        for (my $i=1; $i <= 10; $i++) {
            if ($args->{"key_$i"}) {
                my $key = parse_key($args->{"key_$i"});
                $key_ring->append($key) if $key;
            }
        }

        $args->{system_keys} = $key_ring;

        return $args;
    };

1;

sub BUILD {
    my $self = shift;

    $self->{users} = USK::Users->new(
        usk => $self,
        minimum_uid => $self->minimum_uid,
        maximum_uid => $self->maximum_uid,
        sort_users_by => $self->sort_users_by,
    );

}

sub parse_key {
    my $key_text = shift;
    my $line_num = shift;

    my $key = USK::SSHKey->new(text => $key_text, line_num => $line_num);

    if ($key->is_valid) {
        return $key;
    }

    return undef;
}


package USK::CGI;
use Moo;
use Data::Dumper;

has usk => (is => 'ro', required => 1);
has action => (is => 'ro', required => 1);
has scope => (is => 'ro', default => 'user');
has debug => (is => 'ro', default => 0);
has key => (is => 'ro');
has key_name => (is => 'ro');
has uid => (is => 'ro');
has user => (is => 'ro');
has key_md5 => (is => 'ro');
has selected => (is => 'ro');
has selected_users => (is => 'ro');
has selected_keys => (is => 'ro');
has line_num => (is => 'ro');
has option_cert_authority => (is => 'ro');
has option_command => (is => 'ro');
has option_no_agent_forwarding => (is => 'ro');
has option_no_port_forwarding => (is => 'ro');
has option_no_pty => (is => 'ro');
has option_no_user_rc => (is => 'ro');
has option_no_x11_forwarding => (is => 'ro');
has option_permitopen => (is => 'ro');
has option_principals => (is => 'ro');
has option_tunnel => (is => 'ro');

around BUILDARGS =>
    sub {
        my ($orig, $class, @args) = @_;

        my $args = $class->$orig(@args);

        foreach my $tag (keys %$args) {
            $args->{$tag} =~ s/\s+$//;
        }

        return $args;
    };

our @check_options = qw[
    option_cert_authority
    option_no_agent_forwarding
    option_no_port_forwarding
    option_no_pty
    option_no_user_rc
    option_no_x11_forwarding
];

our @text_options = qw[
    option_command
    option_permitopen
    option_principals
    option_tunnel
];

1;

sub BUILD {
    my $self = shift;

    if (defined $self->uid) {
        $self->{user} = $self->usk->users->get_user($self->uid);
    }

    if ($self->selected_users) {
        my @users;
        foreach my $uid (split /\s*:\s*/, $self->selected_users) {
            my $user = $self->usk->users->get_user($uid);
            push @users, $user if $user;
        }
        $self->{selected_users} = \@users;
    }

    if ($self->selected_keys) {
        my @keys;
        foreach my $md5 (split /\s+/, $self->selected_keys) {
            if ($self->scope eq 'user') {

            }
            push @keys, $md5 if $md5;
        }
        $self->{selected_keys} = \@keys;
    }

    if ($self->key) {
        $self->{key} = USK::SSHKey->new(text => $self->key);

    } elsif ($self->scope eq 'user' && defined $self->uid && $self->key_md5) {
        $self->{key} = $self->user->authorized_keys->get_md5($self->key_md5);

    } elsif ($self->scope eq 'user' && defined $self->uid && $self->line_num) {
        $self->{key} = $self->user->authorized_keys->get_line($self->line_num);
    }
}

sub process_options
{
    my $self = shift;

    if ($self->key) {
        $self->key->{options} = [];

        foreach my $option (@check_options) {
            if ($self->{$option}) {
                my $name = $option;
                $name =~ s/^option_//;
                $name =~ s/_/-/g;

                $self->{key}->add_option(USK::SSHKeyOption->new(name => $name));
            }
        }

        foreach my $option (@text_options) {
            if ($self->{$option}) {
                my $name = $option;
                $name =~ s/^option_//;
                $name =~ s/_/-/g;

                $self->{key}->add_option(USK::SSHKeyOption->new(name => $name, value => $self->{$option}));
            }
        }

        # open(my $fh, '>', 'debug1.txt');
        # print $fh Dumper($self->{key});
        # close $fh;
    }
}

sub requirements_missing
{
    my $self = shift;
    my @requirements = @_;

    foreach my $req (@requirements) {
        if (!$self->$req) {
            return $req;
        }
    }

    return 0;
}

sub requirements_message
{
    my $self = shift;
    my $requirement = shift;

    my $msg = "<p>Required parameter missing: $requirement</p>";

    $msg .= $self->dump() if $self->debug;

    return $msg;
}

sub dump
{
    my $self = shift;

    return '<code style="white-space: pre-wrap">' . Dumper($self) . '</code>';

}
