#!/usr/bin/perl
use warnings;
use strict;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;
use Data::Dumper;
use USK;

require 'user-ssh-keys-lib.pl';

our %text;
our %config;

init_config();

ui_print_header(undef, $text{'index_title'}, "", 'index', 1, 1);

#my $conf = usk_get_config();
my $usk = USK->new(%config);

print "<p><b>Authorized Keys File:</b> $config{authorized_keys_file}</p>\n";

print ui_form_start('manage.cgi', 'post');

#print ui_table_start($text{user_list_heading});
print ui_columns_start(
    [
        '',
        $text{uid},
        $text{user_name},
        $text{user_home}
    ],
    undef,
    0,
    [
        'style="text-align:center; width: 3em;"',
        'align="left"',
        'align="left"',
        'align="left"'
    ],
    #$text{user_list_heading}
);

foreach my USK::User $user ($usk->users->list()) {
    print ui_checked_columns_row(
        [
            ui_link("user.cgi?uid=".urlize($user->uid),
            $user->uid),
            $user->username,
            $user->home
        ],
        [],
        'checked',
        $user->uid,
    );
}

print ui_columns_end();


print ui_form_end();

#$dir = find($conf, "root");
#print &text('index_root', $dir),"<p>\n";
print ui_hr();

print ui_link("keys-list.cgi", $text{keys_by_user_title});

ui_print_footer("/", $text{'index'});
