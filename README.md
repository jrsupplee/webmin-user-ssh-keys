# README

## Description

This Webmin module updates SSH authorized key files for users using Webmin.


## Required PERL Packages

* `Moo`
* `File::Basename`
* `File::Path`
* `Data::Dumper`

## Installation

To install the module download the latest version from [here](https://bitbucket.org/jrsupplee/webmin-user-ssh-keys.git/downloads/) and install it using the Webmin interface.  Make sure to install the required PERL packages listed above.

## Platforms

The module has been tested on Ubuntu Linux.  It should work on Linux platforms that have a single file for all of a user's authorized keys.

## Source Code

The source code repository is located [here](https://bitbucket.org/jrsupplee/webmin-user-ssh-keys.git):

    https://bitbucket.org/jrsupplee/webmin-user-ssh-keys.git

## Reporting Problems

Problems should be reported [here](https://bitbucket.org/jrsupplee/webmin-user-ssh-keys.git/issues).

    https://bitbucket.org/jrsupplee/webmin-user-ssh-keys.git/issues