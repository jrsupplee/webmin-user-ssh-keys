#!/usr/bin/perl

use warnings;
use strict;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;
use Data::Dumper;
use USK;

require 'user-ssh-keys-lib.pl';

our %in;
our %text;
our %config;

ReadParse();
init_config();

my $usk = USK->new( %config );
my $cgi = USK::CGI->new( usk => $usk, %in );

if ($cgi->action eq 'user-key-edit') {
    my $missing = $cgi->requirements_missing('user', 'key');
    if ($missing) {
        error($cgi->requirements_message($missing));
    }

    ui_print_header(undef, $text{key_edit_title}, "");

    #print '<code style="white-space: pre-wrap">', Dumper($key), '</code>';

    key_form($cgi->key);

    ui_print_footer();

} elsif ($cgi->action eq 'user-key-add') {
    my $missing = $cgi->requirements_missing('user');
    if($missing) {
        error($cgi->requirements_message($missing));
    }

    ui_print_header(undef, $text{key_add_title}, "");

    #print '<code style="white-space: pre-wrap">', Dumper($key), '</code>';

    key_form();

    ui_print_footer();
}


sub key_form
{
    my USK::SSHKey $key = shift;

    my $key_name = '';
    my $key_text = '';
    my $action;

    if ($key) {
        $key_name = $key->name;
        $key_text = $key->key;
        $action = 'user-key-update';
    } else {
        $action = 'user-key-create';
    }

    if ($key) {
        print ui_form_start('manage.cgi', 'post');
        print ui_hidden('action', $key->disabled ? 'user-key-enable' : 'user-key-disable');
        print ui_hidden('key_md5', $key->md5);
        print ui_hidden('uid', $cgi->user->uid);

        print ui_submit( $key->disabled ? $text{button_enable} : $text{button_disable});

        print ui_form_end();
        print '<br/>';
    }

    print ui_form_start('manage.cgi', 'post');
    print ui_hidden('action', $action);
    if ($key) {
        print ui_hidden('scope', 'user');
        #print ui_hidden('line_num', $edited_key->line_num);
        #print ui_hidden('key', $edited_key->line);
        print ui_hidden('key_md5', $key->md5);
    }

    print ui_hidden('uid', $cgi->uid);

    print ui_table_start($text{key_edit}, undef, 2);
    print ui_table_row($text{key_name}, ui_textbox('key_name', $key_name));
    print ui_table_row($text{key_value}, ui_textarea('key', $key_text, 6, 60, 'soft', $key));
    print ui_table_end();

    print ui_table_start($text{key_options}, undef, 2);
    print_checkbox_option('cert-authority', $key);
    print_textbox_option('command', $key);
    print_checkbox_option('no-agent-forwarding', $key);
    print_checkbox_option('no-port-forwarding', $key);
    print_checkbox_option('no-pty', $key);
    print_checkbox_option('no-user-rc', $key);
    print_checkbox_option('no-x11-forwarding', $key);
    print_textbox_option('permitopen', $key);
    print_textbox_option('principals', $key);
    print_textbox_option('tunnel', $key);
    print ui_table_end();

    print ui_submit( $key ? $text{button_save_key} : $text{button_add_key});

    print ui_form_end();

    if ($key) {
        print '<br/>';
        print ui_form_start('manage.cgi', 'post');
        print ui_hidden('action', 'user-key-delete');
        print ui_hidden('key_md5', $key->md5);
        print ui_hidden('uid', $cgi->user->uid);

        print ui_submit($text{button_delete_key});

        print ui_form_end();
    }
}

sub print_checkbox_option {
    my $option = shift;
    my $key = shift;

    my $name = $option;

    $name =~ s/-/_/g;

    print ui_table_row($option, ui_checkbox("option_$name", $option, '',  $key->has_option($option)), 2);
}

sub print_textbox_option {
    my $option = shift;
    my $key = shift;

    my $name = $option;

    $name =~ s/-/_/g;

    my $value = '';
    if ($key->has_option($option)) {
        $value = $key->get_option($option)->value;
    }

    print ui_table_row($option, ui_textbox("option_$name", $value, 60), 2);
}
