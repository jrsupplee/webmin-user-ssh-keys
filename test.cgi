#!/usr/bin/perl
use warnings;
use strict;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;
use Data::Dumper;
use USK;

require 'user-ssh-keys-lib.pl';

our %config;
our %text;
our %in;

init_config();
ReadParse();

ui_print_header(undef, $text{'index_title'}, "", 'manage');

#my $conf = usk_get_config();
my $usk = USK->new(%config);
my $cgi = USK::CGI->new(usk => $usk, %in);

print '<code style="white-space: pre-wrap">' . Dumper(\%in) . '</code>';
print '<code style="white-space: pre-wrap">' . Dumper($cgi) . '</code>';
print '<code style="white-space: pre-wrap">' . Dumper($usk) . '</code>';

ui_print_footer("/", $text{'index'});
