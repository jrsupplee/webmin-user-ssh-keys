#!/usr/bin/perl
use warnings;
use strict;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;
use Data::Dumper;
use USK;

require 'user-ssh-keys-lib.pl';

our %in;
our %text;
our %config;

ReadParse();
init_config();

#my $conf = usk_get_config();

my $usk = USK->new(%config);

ui_print_header(undef, $text{keys_by_user_title}, "", 'user');

my USK::KeyList $list = $usk->users->get_key_list();

print ui_form_start('manage.cgi', 'post');

print ui_hidden('scope', 'system');
print ui_hidden('action', 'system-disable-key');

print ui_columns_start(
    [
        '',
        $text{key_name},
        $text{key_type},
        $text{key},
        $text{user_name},
    ],
    undef,
    0,
    ['align="center"', 'align="left"', 'align="left"', 'align="left"'],
    $text{user_list_heading}
);

foreach my USK::KeyListItem $item (@{$list->items}) {
    print ui_checked_columns_row(
            [
                $item->names_string(),
                $item->key->key_type,
                $item->key->abbrev_key,
                $item->users_string(),
            ],
            [],
            'selected_keys',
            $item->key->md5() . ' ',
        );
}

print ui_columns_end();

print ui_submit('Disable', 'submit');

print ui_form_end();

print ui_print_footer();
