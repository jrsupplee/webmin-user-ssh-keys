=head1 user-ssh-keys-lib.pl

Functions for managing the Foobar webserver configuration file.

  foreign_require("foobar", "foobar-lib.pl");
  @sites = foobar::list_foobar_websites()

=cut
use strict;
use warnings;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

BEGIN { push(@INC, ".."); };

use WebminCore;
init_config();

foreign_require('useradmin');

=head2 usk_get_config()

Returns configuration for the module

=cut

sub usk_get_config
{
    return {};
}

1;
