<header>Webmin User SSH Keys Help</header>

## Description

This module modifies a user's <code>authorized_keys</code> file for an OpenSSH or equivalent server.  The module attempts to leave comment lines and blank lines in the file

## Security Considerations

Any program that modifies user access ability presents security risks.  System administrators should carefully consider these risks before enabling this module.

## Adding or Modifying Keys

Keys can be added or modified using the Webmin interface.  The name of a key is taken to be the comment text that appears at the end of a key.  Comments at the end of keys will be overwritten with key name in the interface.

## Configuration

*Authorized Keys File Path:*

: path to a user's authorized keys file.  A tilde (`~`) will be replaced with the user's home directory.  If this value is blank, it will cause errors when trying to add keys to users.

    Default: `~/.ssh/authorized_keys`

*Minimum UID:*

: the minimum numeric UID for display of users.  Users with a UID less than this number will not be displayed in the user list for the module.
  
    Default: `1000`

*Maximum UID:*

: the maximum UID for display of users.  Users with a UID greater than this number will not be displayed in this list.

    Default: `10000`
    
*System Key #1 -- #6:*

: these keys allow for the storage of public keys that can be quickly added to any user.  The text box should include the text of the public key *plus* a comment describing it at the end of the line (*e.g.* an email address).

