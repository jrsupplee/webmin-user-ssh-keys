#!/usr/bin/perl
use warnings;
use strict;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;
use Data::Dumper;
use USK;

require 'user-ssh-keys-lib.pl';

our %config;
our %text;
our %in;

init_config();
ReadParse();

my $usk = USK->new( %config );

my $cgi = USK::CGI->new(usk => $usk, %in);

if ($cgi->action eq 'user-key-create') {
    my $missing = $cgi->requirements_missing('user', 'key_name', 'key');
    if($missing) {
        error($cgi->requirements_message($missing));
    }

    my USK::SSHKey $key = $cgi->key;
    $key->name($cgi->key_name);

    $cgi->user->authorized_keys->add_key($key);
    $cgi->user->authorized_keys->write_changes();

    redirect('user.cgi?uid=' . $cgi->user->uid);

} elsif ($cgi->action eq 'user-key-update') {
    # open(my $fh, '>', 'debug.txt');
    # print $fh Dumper(\%in);
    # close $fh;
    my $missing = $cgi->requirements_missing('user', 'key_name', 'key');
    if($missing) {
        error($cgi->requirements_message($missing));
    }

    $cgi->process_options();
    my USK::SSHKey $key = $cgi->key;
    $key->name($cgi->key_name);
    $cgi->user->authorized_keys->update_key($cgi->key);
    $cgi->user->authorized_keys->write_changes();

    redirect('user.cgi?uid=' . $cgi->user->uid);

} elsif ($cgi->action eq 'user-key-delete') {
    my $missing = $cgi->requirements_missing('user', 'key');
    if ($missing) {
        error($cgi->requirements_message($missing));
    }

    $cgi->user->authorized_keys->delete_key($cgi->key);
    $cgi->user->authorized_keys->write_changes();

    redirect('user.cgi?uid='.$cgi->user->uid);

} elsif ($cgi->action eq 'user-key-enable') {
    my $missing = $cgi->requirements_missing('user', 'key');
    if ($missing) {
        error($cgi->requirements_message($missing));
    }

    #$cgi->key->disabled(0);
    $cgi->user->authorized_keys->enable_key($cgi->key);
    $cgi->user->authorized_keys->write_changes();

    redirect('user.cgi?uid='.$cgi->user->uid);

} elsif ($cgi->action eq 'user-key-disable') {
    my $missing = $cgi->requirements_missing('user', 'key');
    if ($missing) {
        error($cgi->requirements_message($missing));
    }

    $cgi->user->authorized_keys->disable_key($cgi->key);
    $cgi->user->authorized_keys->write_changes();

    redirect('user.cgi?uid='.$cgi->user->uid);

} elsif ($cgi->action eq 'user-add-system-key') {
    my $missing = $cgi->requirements_missing('user', 'key_md5');
    if($missing) {
        error($cgi->requirements_message($missing));
    }

    my USK::SSHKey $key = $usk->system_keys->get_md5($cgi->key_md5);

    if ($key) {
        $cgi->user->authorized_keys->add_key($key);
        $cgi->user->authorized_keys->write_changes();

    } else {
        error('Key MD5 not found: ' . $cgi->key_md5);
    }

    redirect('user.cgi?uid=' . $cgi->user->uid);

} elsif ($cgi->action eq 'system-disable-key') {
    my $missing = $cgi->requirements_missing('selected_keys');
    if($missing) {
        error($cgi->requirements_message($missing));
    }

    foreach my USK::User $user ($usk->users->list()) {
        foreach my $md5 (@{$cgi->selected_keys}) {
            $user->authorized_keys->disable_key($md5);
        }
        $user->authorized_keys->write_changes();
    }

} else {
    error('Undefined action: ' . $cgi->action);

}

redirect('index.cgi');
