
WEBMIN_ROOT=/usr/share/webmin

wbm:
	cd $(WEBMIN_ROOT); \
	echo $$PWD; \
	tar czvf /tmp/user-ssh-keys-`git -C user-ssh-keys describe --tags`.wbm.gz --exclude=.git* --exclude=.idea --exclude=_* user-ssh-keys/
